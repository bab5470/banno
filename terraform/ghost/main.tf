provider "kubernetes" {}

resource "kubernetes_pod" "ghost" {
  metadata {
    name = "ghost"
    labels {
      app = "ghost"
    }
  }

  spec {
    container {
      image = "ghost"
      name  = "ghost"

      port {
        container_port = 2368
      }
    }
  }
}

resource "kubernetes_service" "ghost" {
  metadata {
    name = "ghost"
  }
  spec {
    selector {
      app = "${kubernetes_pod.ghost.metadata.0.labels.app}"
    }
    session_affinity = "ClientIP"
    port {
      port = 80
      target_port = 2368
    }

    type = "ClusterIP"
  }
}

