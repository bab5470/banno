provider "kubernetes" {}

resource "kubernetes_pod" "echo-server" {
  metadata {
    name = "echo-server"
    labels {
      app = "echo-server"
    }
  }

  spec {
    container {
      image = "k8s.gcr.io/echoserver:1.10"
      name  = "echo-server"

      port {
        container_port = 8080
      }
    }
  }
}

resource "kubernetes_service" "echo-server" {
  metadata {
    name = "echo-server"
  }
  spec {
    selector {
      app = "${kubernetes_pod.echo-server.metadata.0.labels.app}"
    }
    session_affinity = "ClientIP"
    port {
      port = 80
      target_port = 8080
    }

    type = "ClusterIP"
  }
}

