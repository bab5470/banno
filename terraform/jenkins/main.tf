provider "kubernetes" {}

resource "random_string" "password" {
  length = 32
  lower = true
  upper = false
  special = false
}

resource "kubernetes_pod" "jenkins" {
  metadata {
    name = "jenkins"
    labels {
      app = "jenkins"
    }
  }

  spec {
    container {
      image = "jenkins:latest"
      name  = "jenkins"

      port {
        container_port = 8080
      }
      env {
       name = "JENKINS_OPTS"
       value = "--prefix=/jenkins"
      }
    }
  }
}

resource "kubernetes_service" "jenkins" {
  metadata {
    name = "jenkins"
  }
  spec {
    selector {
      app = "${kubernetes_pod.jenkins.metadata.0.labels.app}"
    }
    session_affinity = "ClientIP"
    port {
      port = 80
      target_port = 8080
    }

    type = "ClusterIP"
  }
}

output "password" { value = "${random_string.password.result}" }
