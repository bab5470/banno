# Install terraform
sudo curl -O  https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
sudo unzip terraform_0.11.8_linux_amd64.zip -d /usr/local/bin/ 
terraform --version

# Install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
mkdir ~/.kube/
cp ../config/certs/config ~/.kube/config
kubectl get nodes

# Install helm
sudo curl -O  https://storage.googleapis.com/kubernetes-helm/helm-v2.10.0-linux-amd64.tar.gz
sudo tar -xvf helm-v2.10.0-linux-amd64.tar.gz -C /tmp/
sudo chmod +x /tmp/linux-amd64/helm 
sudo mv /tmp/linux-amd64/helm /usr/local/bin/

# Configure kubernetes for helm and tiller
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

# Create keys 
# Only run this once to create new certs.
# openssl genrsa -out ../config/certs/ca.key.pem 4096
# openssl req -key ../config/certs/ca.key.pem -new -x509 -days 7300 -sha256 -out ../config/certs/ca.cert.pem -extensions v3_ca
# openssl genrsa -out ../config/certs/tiller.key.pem 4096
# openssl genrsa -out ../config/certs/helm.key.pem 4096
# openssl req -key ../config/certs/tiller.key.pem -new -sha256 -out ../config/certs/tiller.csr.pem -config <( cat ../config/certs/csr_details.txt )
# openssl req -key ../config/certs/helm.key.pem -new -sha256 -out ../config/certs/helm.csr.pem
# openssl x509 -req -CA ../config/certs/ca.cert.pem -CAkey ../config/certs/ca.key.pem -CAcreateserial -in ../config/certs/tiller.csr.pem -out ../config/certs/tiller.cert.pem -days 365
# openssl x509 -req -CA ca.cert.pem -CAkey ca.key.pem -CAcreateserial -in tiller.csr.pem -out tiller.cert.pem -days 365 -extfile csr_details.txt -extensions 'req_ext'

# Copy over pre-created keys
mkdir ~/.helm
cp ../config/certs/ca.cert.pem ~/.helm/ca.pem
cp ../config/certs/helm.cert.pem ~/.helm/cert.pem
cp ../config/certs/helm.key.pem ~/.helm/key.pem

# Configure helm
helm init --tiller-tls --tiller-tls-cert ../config/certs/tiller.cert.pem --tiller-tls-key ../config/certs/tiller.key.pem --tiller-tls-verify --tls-ca-cert ../config/certs/ca.cert.pem --tiller-namespace=kube-system --service-account=tiller
helm repo update

# Get the helm terraform plugin
sudo curl -LO https://github.com/mcuadros/terraform-provider-helm/releases/download/v0.6.0/terraform-provider-helm_v0.6.0_linux_amd64.tar.gz
sudo tar -xvf terraform-provider-helm_v0.6.0_linux_amd64.tar.gz -C /tmp
mkdir ~/.terraform.d/plugins
mv /tmp/terraform-provider-helm*/terraform-provider-helm ~/.terraform.d/plugins/

# Install Cert-Manager
helm install --name cert-manager stable/cert-manager --tls
kubectl create -f ../config/cert-manager/cert-manager-certificate.yaml
kubectl create -f ../config/cert-manager/cert-manager-clusterissuer.yaml


